<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 31.5.18
 * Time: 02:35 AM
 */
class ApartmentModel extends CI_Model
{
    public function saveApt($apt)
    {
        $price = null;
        $priceDetails = null;
        $aptDetails = null;
        $location = null;

        if (array_key_exists("price", $apt)) {
            $price = $apt['price'];
            unset($apt['price']);
            if (array_key_exists("priceDetails", $price)) {
                $priceDetails = $price['priceDetails'];
                unset($price['priceDetails']);
            }
        }
        if (array_key_exists("apartmentDetails", $apt)) {
            $aptDetails = $apt['apartmentDetails'];
            unset($apt['apartmentDetails']);
        }
        if (array_key_exists("location", $apt)) {
            $location = $apt['location'];
            unset($apt['location']);
        }

        $this->db->trans_start();
        $this->db->trans_strict(FALSE);

        $this->db->insert("location", $location);
        $locId = $this->db->insert_id();
        $apt['locationId'] = $locId;
        $this->db->insert("apartment", $apt);
        $aptId = $this->db->insert_id();
        $aptDetails['apartmentId'] = $aptId;
        $this->db->insert("apartment_details", $aptDetails);
        $price['apartmentId'] = $aptId;
        $this->db->insert("price", $price);
        $priceDetails['priceId'] = $this->db->insert_id();
        $this->db->insert("price_details", $priceDetails);
        $this->db->trans_complete();
        return $aptId;
    }

    public function updateApt($apt)
    {
        $price = null;
        $priceDetails = null;
        $aptDetails = null;
        $location = null;

        if (array_key_exists("price", $apt)) {
            $price = $apt['price'];
            unset($apt['price']);
            if (array_key_exists("priceDetails", $price)) {
                $priceDetails = $price['priceDetails'];
                unset($price['priceDetails']);
            }
        }
        if (array_key_exists("apartmentDetails", $apt)) {
            $aptDetails = $apt['apartmentDetails'];
            unset($apt['apartmentDetails']);
        }
        if (array_key_exists("location", $apt)) {
            $location = $apt['location'];
            unset($apt['location']);
        }

        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        if ($location != null) {
            $this->db->where("locationId", $location['locationId'])->update("location", $location);
            $locId = $location['locationId'];
            $apt['locationId'] = $locId;
        }
        $this->db->where("appartmentId", $apt['appartmentId'])->update("apartment", $apt);
        if ($aptDetails != null) {
            $this->db->where("apartmentDetailsId", $aptDetails['apartmentDetailsId'])->update("apartment_details", $aptDetails);
        }
        if ($price != null)
            $this->db->where("priceId", $price["priceId"])->update("price", $price);
        if ($priceDetails != null)
            $this->db->where("priceDetailsId", $priceDetails["priceDetailsId"])->update("price_details", $priceDetails);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            return $apt["appartmentId"];
        } else {
            return 0;
        }
    }

    public function getById($id)
    {
        $apt = $this->db->select("*")->from("apartment")->where("appartmentId", $id)->get()->row();
        if ($apt != null) {
            $aptDet = $this->db->select("*")->from("apartment_details")->where("apartmentId", $id)->get()->row();
            if ($aptDet != null) {
                $apt->apartmentDetails = $aptDet;
            }
            $user = $this->db->select("*")->from("user")->where("userId", $apt->userId)->get()->row();
            if ($user != null) {
                $apt->user = $user;
                $role = $this->db->select("*")->from("user_role")->where("userRoleId", $user->userRoleId)->get()->row();
                if ($role != null)
                    $user->userRole = $role;
            }
            $loc = $this->db->select("*")->from("location")->where("locationId", $apt->locationId)->get()->row();
            if ($loc != null) {
                $apt->location = $loc;
            }
            $ats = $this->db->select("*")->from("attachments")->where("apartmentId", $apt->appartmentId)->get()->result();

            if ($ats != null) {
                $apt->attachments = $ats;
            }
            $price = $this->db->select("*")->from("price")->where("apartmentId", $apt->appartmentId)->get()->row();
            if ($price != null) {
                $apt->price = $price;
                $priceDet = $this->db->select("*")->from("price_details")->where("priceId", $price->priceId)->get()->row();
                if ($priceDet != null) {
                    $price->priceDetails = $priceDet;
                }
            }
            return $apt;
        }
        return null;
    }

    public function getApartmentsByUserId($userId)
    {
        $apt = $this->db->select("*")->from("apartment")->where("userId", $userId)->get()->result();
        if ($apt != null) {
            foreach ($apt as $apartment) {
                $aptDet = $this->db->select("*")->from("apartment_details")->where("apartmentId", $apartment->appartmentId)->get()->row();
                if ($aptDet != null) {
                    $apartment->apartmentDetails = $aptDet;
                }
                $user = $this->db->select("*")->from("user")->where("userId", $apartment->userId)->get()->row();
                if ($user != null) {
                    $apartment->user = $user;
                    $role = $this->db->select("*")->from("user_role")->where("userRoleId", $user->userRoleId)->get()->row();
                    if ($role != null)
                        $user->userRole = $role;
                }
                $loc = $this->db->select("*")->from("location")->where("locationId", $apartment->locationId)->get()->row();
                if ($loc != null) {
                    $apartment->location = $loc;
                }
                $price = $this->db->select("*")->from("price")->where("apartmentId", $apartment->appartmentId)->get()->row();
                if ($price != null) {
                    $apartment->price = $price;
                    $priceDet = $this->db->select("*")->from("price_details")->where("priceId", $price->priceId)->get()->row();
                    if ($priceDet != null) {
                        $price->priceDetails = $priceDet;
                    }
                }
            }
            return $apt;
        }
        return null;
    }

    public function countApartmentByUserId($userId)
    {
        $totalApt = $this->db->select("*")->from("apartment")->where("userId", $userId)->get()->num_rows();
        return $totalApt;
    }
}