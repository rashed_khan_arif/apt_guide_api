<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 30.12.17
 * Time: 12:11 AM
 */
class UserReviewModel extends CI_Model
{
    public function insert($data)
    {

        $res = $this->db->insert("review", $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $res = $this->db->where("reviewId", $data['reviewId'])->update("review", $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public function get($id)
    {
        $review = $this->db->select("*")->from("review")->where("reviewId", $id)->get()->row();
        if (!is_null($review)) {
            return $review;
        }
        return null;
    }

    public function getAll()
    {
        $reviews = $this->db->select("*")->from("review")->get()->result();
        if (!is_null($reviews)) {
            return $reviews;
        }
        return null;
    }

    public function getAllByUserId($userId, $skip, $top)
    {
        $reviews = $this->db->select("*")->from("review")->where("reviewedByUserId", $userId)->order_by("reviewDate", "DESC")->limit($top, $skip)->get()->result();
        if (!is_null($reviews)) {
            return $reviews;
        }
        return null;
    }

    public function getAllByAptId($apartmentId, $skip, $top)
    {
        $reviews = $this->db->select("*")->from("review")->where("apartmentId", $apartmentId)->order_by("reviewDate", "DESC")->limit($top, $skip)->get()->result();
        if (!is_null($reviews)) {
            return $reviews;
        }
        return null;
    }
}