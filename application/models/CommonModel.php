<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 31.5.18
 * Time: 09:03 PM
 */
class CommonModel extends CI_Model
{
    public function getDivisions()
    {
        $data = $this->db->select("*")->from("divisions")->get()->result();

        return $data;
    }

    public function getDistricts($divId)
    {
        $data = $this->db->select("*")->from("districts")->where("division_id", $divId)->get()->result();
        return $data;
    }

    public function getUpazilas($disId)
    {
        $data = $this->db->select("*")->from("upazilas")->where("district_id", $disId)->get()->result();
        return $data;
    }
    public function getLocations($upzId)
    {
        $data = $this->db->select("*")->from("locations")->where("upozilaId", $upzId)->get()->result();
        return $data;
    }

}