<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 11.12.17
 * Time: 01:25 AM
 */
class UserModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("ApartmentModel", "aptModel");
    }

    function get($id)
    {
        $query = $this->db->select("*")->from("user")->where("userId", $id);
        $data = $query->get()->row();
        if ($data) {
            unset($data->password);
            $userRole = $this->db->select("*")->from("user_role")->where("userRoleId", $data->userRoleId)->get()->row();
            if (!empty($userRole)) {
                $data->userRole = $userRole;
            }
            $countApt = $this->aptModel->countApartmentByUserId($data->userId);
            $data->totalApartments = $countApt;
            return $data;
        }
        return null;
    }

    function getAll()
    {
        $datas = $this->db->select("*")->from("user")
            ->get()->result();

        if (isset($datas)) {
            foreach ($datas as $data) {
                unset($data->password);
                $userRole = $this->db->select("*")->from("user_role")->where("userRoleId", $data->userRoleId)->get()->row();
                $data->userRole = $userRole;
            }
            return $datas;
        } else {
            return null;
        }

    }

    function insert($data)
    {
        $res = $this->db->insert("user", $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    function update($data)
    {

        $res = $this->db->where("userId", $data['userId'])->update("user", $data);

        if ($res) {
            return true;
        } else {
            return false;
        }

    }

}