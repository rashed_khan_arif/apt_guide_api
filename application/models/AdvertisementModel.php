<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 31.5.18
 * Time: 02:35 AM
 */
class AdvertisementModel extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("ApartmentModel", "aptModel");
    }

    public function search($divId, $disId, $upzId, $adType)
    {
        $data = $this->db->select("ad.*")
            ->from("advertisement ad")
            ->join("upazilas u", "ad.upozilaId=u.upozilaId", "left")
            ->join("districts d", "u.district_id=d.district_id", "left")
            ->where("ad.adType", $adType)
            ->where("ad.isPublished", 1)
            ->or_where("ad.upozilaId", $upzId)
            ->or_where("u.district_id", $disId)
            ->or_where("d.division_id", $divId);
        $res = $data->get()->result();
        if ($res != null) {
            foreach ($res as $rt) {
                $apt = $this->aptModel->getById($rt->apartmentId);
                if ($apt != null) {
                    $rt->apartment = $apt;
                }
            }
        }
        return $res;
    }

    public function getTop($adType)
    {
        $data = $this->db->select("ad.*")
            ->from("advertisement ad")
            ->join("upazilas u", "ad.upozilaId=u.upozilaId", "left")
            ->join("districts d", "u.district_id=d.district_id", "left")
            ->where("ad.adType", $adType)
            ->where("ad.isPublished", 1)
            ->order_by("ad.advertisementId", "DESC");
        $res = $data->get()->result();
        if ($res != null) {
            foreach ($res as $rt) {
                $apt = $this->aptModel->getById($rt->apartmentId);
                if ($apt != null) {
                    $rt->apartment = $apt;
                }
            }
        }
        return $res;
    }

    public function get($id)
    {
        $data = $this->db->select("ad.*")
            ->from("advertisement ad")
            ->where("ad.advertisementId", $id);
        $rt = $data->get()->row();
        if ($rt != null) {
            $apt = $this->aptModel->getById($rt->apartmentId);
            if ($apt != null) {
                $rt->apartment = $apt;
            }
        }
        return $rt;
    }

    public function getByUserId($id)
    {
        $data = $this->db->select("ad.*")
            ->from("advertisement ad")
            ->where("ad.userId", $id);
        $res = $data->get()->result();

        if ($res != null) {
            foreach ($res as $rt) {
                $apt = $this->aptModel->getById($rt->apartmentId);
                if ($apt != null) {
                    $rt->apartment = $apt;
                }
            }

        }
        return $res;
    }

    public function create($adv)
    {
        $this->db->insert("advertisement", $adv);
        $res = $this->db->insert_id();
        if ($res != 0) {
            return true;
        } else return false;
    }

    public function updateStatus($adId, $status)
    {
        $this->db->set("isPublished", $status)->where("advertisementId", $adId)->update("advertisement");
        return true;
    }
}