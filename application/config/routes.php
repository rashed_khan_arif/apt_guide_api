<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//For version- v1
$route['Account/register'] = 'api/LoginController/accountCreate';
$route['api/v1/apartments-by-id'] = 'api/ApartmentController/getApartmentById';
$route['api/v1/apartments-by-user-id'] = 'api/ApartmentController/getApartmentByUserId';
$route['api/v1/apartments'] = 'api/ApartmentController/getApartments';
$route['api/v1/apartments-add'] = 'api/ApartmentController/addApartments';
$route['api/v1/advertisements-by-id'] = 'api/AdvertisementController/getAdvertisementById';
$route['api/v1/advertisements'] = 'api/AdvertisementController/getAdvertisements';
$route['api/v1/advertisements-by-user'] = 'api/AdvertisementController/getAdvertisementsByUserId';
$route['api/v1/advertisements-add'] = 'api/AdvertisementController/addAdvertisements';
$route['api/v1/update-status'] = 'api/AdvertisementController/updateStatusOfAd';
$route['api/v1/authentication'] = 'api/LoginController/authorization';
$route['api/v1/users/add-or-update'] = 'api/UserController/addOrUpdateUsers';
$route['api/v1/user-by-id'] = 'api/UserController/userDetails';
$route['api/v1/users'] = 'api/UserController/users';
$route['api/v1/divisions'] = 'api/CommonController/getDivisions';
$route['api/v1/districts'] = 'api/CommonController/getDistricts';
$route['api/v1/upozilas'] = 'api/CommonController/getUpazilas';
$route['api/v1/locations'] = 'api/CommonController/getLocations';
$route['api/v1/upload-image'] = 'api/ApartmentController/uploadUserImage';
$route['api/v1/reviews-by-apt-id'] = 'api/UserReviewController/getReviewByAptId';
$route['api/v1/reviews-by-user-id'] = 'api/UserReviewController/getReviewByUserId';
$route['api/v1/add-review'] = 'api/UserReviewController/addOrUpdateUserReview';
$route['api/v1/send-code'] = 'api/LoginController/sendCode';
$route['api/v1/check-code'] = 'api/LoginController/checkCode';