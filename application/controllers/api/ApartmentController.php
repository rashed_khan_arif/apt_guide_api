<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 31.5.18
 * Time: 02:41 AM
 */
require 'Base_Api_Controller.php';

class ApartmentController extends Base_Api_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model("ApartmentModel", "apartment");
    }

    public function getApartments_get()
    {

    }

    public function getApartmentByUserId_get()
    {
        $userId = $this->get("userId");

        if ($userId == 0) {
            $this->response("Bad request", REST_Controller::HTTP_BAD_REQUEST);
        }
        $apartments = $this->apartment->getApartmentsByUserId($userId);
        if ($apartments == null) {
            $this->response("No Content Found ", REST_Controller::HTTP_NO_CONTENT);
        }
        $this->response($apartments, REST_Controller::HTTP_OK);

    }

    public function addApartments_post()
    {
        $apartment = $this->request->body;
        if ($apartment == null) {
            $this->response("Bad Request", REST_Controller::HTTP_BAD_REQUEST);
        }

        if (array_key_exists("user", $apartment)) {
            unset($apartment["user"]);
        }

        if (array_key_exists("advertisements", $apartment)) {
            unset($apartment["advertisements"]);
        }
        $aptId = 0;
        if ($apartment['appartmentId'] == 0) {
            $aptId = $this->apartment->saveApt($apartment);
        } else {
            $aptId = $this->apartment->updateApt($apartment);
        }
        if ($aptId == 0) {
            $this->response("Failed to save", REST_Controller::HTTP_BAD_REQUEST);
        } else $this->response($aptId, REST_Controller::HTTP_CREATED);
    }

    public function uploadUserImage_post()
    {
        $item = $this->post("body");
        $body = json_decode($item, true);
        $fileName = $body['fileName'];
        $config['upload_path'] = APPPATH . '../image/';
        $config['allowed_types'] = 'gif|jpg|png|doc|txt';
        $config['file_name'] = $fileName;
        $config['max_size'] = 204800000000;
        $config['max_width'] = 100024;
       $config['max_height'] = 70068;
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $this->response("Failed to upload !", REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $apartmentId = $body['apartmentId'];
            $image = array();
            $image['fileName'] = $fileName;
            $image['apartmentId'] = $apartmentId;
            $image['attachType'] = $body['attachType'];
            $this->db->insert("attachments", $image);
            $this->response("Uploaded", REST_Controller::HTTP_OK);
        }
    }

}