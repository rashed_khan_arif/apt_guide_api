<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 31.5.18
 * Time: 09:03 PM
 */

require 'Base_Api_Controller.php';

class CommonController extends Base_Api_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model("CommonModel", "cm");
    }

    public function getDivisions_get()
    {
        $data = $this->cm->getDivisions();
        if ($data == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function getDistricts_get()
    {
        $divId = $this->get("divisionId");
        if ($divId == null or $divId == 0) {
            $this->response("No Content", REST_Controller::HTTP_BAD_REQUEST);
        }
        $data = $this->cm->getDistricts($divId);
        if ($data == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function getUpazilas_get()
    {
        $disId = $this->get("districtId");
        if ($disId == null or $disId == 0) {
            $this->response("No Content", REST_Controller::HTTP_BAD_REQUEST);
        }
        $data = $this->cm->getUpazilas($disId);
        if ($data == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function getLocations_get()
    {
        $upzId = $this->get("upzId");
        if ($upzId == null or $upzId == 0) {
            $this->response("No Content", REST_Controller::HTTP_BAD_REQUEST);
        }
        $data = $this->cm->getLocations($upzId);
        if ($data == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
}