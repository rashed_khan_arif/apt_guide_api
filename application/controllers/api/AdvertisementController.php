<?php

/**
 * Created by PhpStorm.
 * User: arifk
 * Date: 31.5.18
 * Time: 02:33 AM
 */
require 'Base_Api_Controller.php';

class AdvertisementController extends Base_Api_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model("AdvertisementModel", "adModel");
    }

    public function getAdvertisements_get()
    {
        $this->isAuth();
        $divId = $this->get("divId");
        $disId = $this->get("disId");
        $upzId = $this->get("upzId");
        $adType = $this->get("adType");
        $top = $this->get("top");

        if ($divId == 0 && $disId == 0 && $upzId == 0 && $adType == 0) {

            $this->response($adType, REST_Controller::HTTP_BAD_REQUEST);
        }
        if ($top == true) {
            $apts = $this->adModel->getTop($adType);
            $this->response($apts, REST_Controller::HTTP_OK);
        }

        $apts = $this->adModel->search($divId, $disId, $upzId, $adType);
        if ($apts == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        } else {
            $this->response($apts, REST_Controller::HTTP_OK);
        }
    }

    public function getAdvertisementsByUserId_get()
    {
        $this->isAuth();

        $userId = $this->get("userId");
        if ($userId == 0) {
            $this->response("Bad request ...", REST_Controller::HTTP_BAD_REQUEST);
        }
        $apts = $this->adModel->getByUserId($userId);
        if ($apts == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        } else {
            $this->response($apts, REST_Controller::HTTP_OK);
        }
    }

    public function getAdvertisementById_get()
    {
        $this->isAuth();
        $id = $this->get("id");
        if ($id == 0) {
            $this->response("Bad Request", REST_Controller::HTTP_BAD_REQUEST);
        }
        $apts = $this->adModel->get($id);
        if ($apts == null) {
            $this->response("No Content", REST_Controller::HTTP_NO_CONTENT);
        } else {
            $this->response($apts, REST_Controller::HTTP_OK);
        }
    }

    public function addAdvertisements_post()
    {
        $adv = $this->request->body;
        if ($adv == null) {
            $this->response("Bad Request", REST_Controller::HTTP_BAD_REQUEST);
        }
        if (array_key_exists("apartment", $adv)) {
            unset($adv["apartment"]);
        }
        $res = $this->adModel->create($adv);
        if (!$res) {
            $this->response("Failed to save", REST_Controller::HTTP_BAD_REQUEST);
        } else $this->response("Created", REST_Controller::HTTP_CREATED);
    }

    public function updateStatusOfAd_post()
    {
        $adId = $this->post("adId");
        $status = $this->post("status");
        $res = $this->adModel->updateStatus($adId, $status);
        if (!$res) {
            $this->response("Failed", REST_Controller::HTTP_BAD_REQUEST);
        } else $this->response("Success", REST_Controller::HTTP_OK);
    }


}